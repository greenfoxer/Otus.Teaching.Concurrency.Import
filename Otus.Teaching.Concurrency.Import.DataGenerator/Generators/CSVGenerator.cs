﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CSVGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CSVGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            System.Console.WriteLine("Start generation");
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.CreateText(_fileName);
            foreach (var item in customers)
            {
                stream.WriteLine(item.ToString());
            }
            System.Console.WriteLine("End generation");
        }

        public async Task GenerateAsync()
        {
            await Task.Run(() => Generate());
        }
    }
}