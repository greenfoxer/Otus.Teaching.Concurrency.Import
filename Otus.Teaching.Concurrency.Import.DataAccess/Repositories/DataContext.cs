﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class DataContext
        : DbContext
    {
        string database = "HomeWorkCustomers.db";
        string file { get { return "Filename =" + database; } }
        public DataContext()
        {
            //this.Database.EnsureDeleted();
            //this.Database.EnsureCreated();
        }
        public DataContext(string path)
        {
            database = path;
        }
        public void RecreateDB()
        {
            this.Database.EnsureDeleted();
            this.Database.EnsureCreated();
        }

        public DbSet<Customer> Customers { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(file);
        }
    }
}
