﻿using System;
using System.Collections.Generic;
using System.Text;
using System;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerCacheItem
    {
        public Customer Customer;
        public double Rating;
        public int CountMentions = 0;
        int LastMention = 0;

        public CustomerCacheItem(Customer item, int reqnum)
        {
            this.Customer = item;
            this.Rating = 100;
            this.LastMention = reqnum;
            this.CountMentions++;
        }

        public async Task UpdateRating(int totalnum=0)
        {
            int CurrentRequest = 0 ;
            if (totalnum != 0)  // если параметр по умолчанию - то просто увеличиваем количество упоминаний и обновляем номер последнего упоминания
                CurrentRequest = totalnum;
            else
            {
                CountMentions++;
                LastMention = CurrentRequest;
                Rating = 100;
            }
            if(CurrentRequest != LastMention) // понижаем рейтинг
            {
                Rating-=2.0/CountMentions; // снижаем рейтинг на 2/Количество упоминаний. Часто упоминаемые будут дольше вылетать из рейтинга.
            }
        }
    }
}
