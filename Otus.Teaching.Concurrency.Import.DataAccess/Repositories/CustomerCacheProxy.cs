﻿using System;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Collections.Concurrent;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerCacheProxy
        : ICustomerRepository, IDisposable
    {
        public CustomerCacheProxy(ICustomerRepository repo)
        {
            repository = repo;
            _cache2 = new Dictionary<int, CustomerCacheItem>();
            TotalCollectionNum = repo.GetAllUsersAsync().Result.Count;
        }
        DataContext dataContext;
        private ICustomerRepository repository;

        public int AddCustomer(Customer customer)
        {
            return repository.AddCustomer(customer);
        }

        public void AddCustomer(IEnumerable<Customer> customers)
        {
            repository.AddCustomer(customers);
        }

        public async Task AddCustomerAsync(IEnumerable<Customer> customers)
        {
            await repository.AddCustomerAsync(customers);
        }

        public async Task<int> AddCustomerAsync(Customer customer)
        {
            return await repository.AddCustomerAsync(customer);
        }

        public void Dispose()
        {
            //repository.Dispose();
        }

        public async Task<List<Customer>> GetAllUsersAsync()
        {
            return await repository.GetAllUsersAsync();
        }

        int TotalCollectionNum;
        readonly Dictionary<int, CustomerCacheItem> _cache2;
        readonly int ParettoPercentage = 80; // Для определения самых популярных запросов воспользуемся обычным правилом Паретто: будем держать в кэше только 20 про
        async Task UpdateCache()
        {
            foreach (var item in _cache2) // обновим рейтинги всех нод
                await item.Value.UpdateRating(RequestNum);
            if (_cache2.Count > TotalCollectionNum / 5) // чистим кэш при превышении его длины 20% оригинальных данных
            {
                var ratings = _cache2.Select(t => new { t.Key, t.Value.CountMentions, t.Value.Rating }).ToArray();
                foreach (int key in _cache2.Where(t => t.Value.Rating < ParettoPercentage).Select(p => p.Key))
                {
                    _cache2.Remove(key);
                }
            }
        }
        int RequestNum = 0;
        public async Task<Customer> GetUserAsync(int id)
        {
            Interlocked.Increment(ref RequestNum);
            if (!_cache2.ContainsKey(id))
                _cache2.Add(id, new CustomerCacheItem(await repository.GetUserAsync(id),RequestNum));
            else
                await _cache2[id].UpdateRating();
            await UpdateCache();
            return _cache2[id].Customer;
        }
    }
}
