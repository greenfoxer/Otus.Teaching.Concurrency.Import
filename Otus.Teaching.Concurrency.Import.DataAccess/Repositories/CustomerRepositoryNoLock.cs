﻿using System;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepositoryNoLock
        : ICustomerRepository, IDisposable
    {
        DataContext dataContext;

        public CustomerRepositoryNoLock()
        {
            dataContext = new DataContext();
        }

        public CustomerRepositoryNoLock(string path)
        {
            dataContext = new DataContext(path);
        }

        public CustomerRepositoryNoLock(DataContext context)
        {
            dataContext = context;
        }

        public void TruncateTable()
        {
            dataContext.RecreateDB();
        }

        public void AddCustomer(IEnumerable<Customer> customers)
        {
            dataContext.Customers.AddRange(customers);
            dataContext.SaveChanges();
        }

        public void RemoveCustomers(IEnumerable<Customer> customers)
        {
            dataContext.Customers.RemoveRange(customers);
            dataContext.SaveChanges();
        }

        public async Task AddCustomerAsync(IEnumerable<Customer> customers)
        {
            dataContext.Customers.AddRange(customers);
            await dataContext.SaveChangesAsync();
        }

        public int AddCustomer(Customer customer)
        {
            if (!dataContext.Customers.Contains(customer))
            {
                dataContext.Customers.Add(customer);
                dataContext.SaveChanges();
                return 0;
            }
            else
            {
                //Console.WriteLine($"{customer.ToString()} already in database");
                return 1;
            }
        }
        public async Task<int> AddCustomerAsync(Customer customer)
        {
            if (!dataContext.Customers.Contains(customer))
            {
                dataContext.Customers.Add(customer);
                await dataContext.SaveChangesAsync();
                return 0;
            }
            else
            {
                return 1;
            }
        }
        public async Task<List<Customer>> GetAllUsersAsync()
        {
            return await dataContext.Customers.ToListAsync();
        }
        public async Task<Customer> GetUserAsync(int id)
        {
            //var d = await dataContext.Customers.Select(p => p.ToString()).ToListAsync();
            return await dataContext.Customers.FirstOrDefaultAsync(p=>p.Id==id);
        }
        public List<string> GetAllUsers()
        {
            //var d = await dataContext.Customers.Select(p => p.ToString()).ToListAsync();
            var data = dataContext.Customers.Select(p => p.ToString());
            return data.ToList();
        }
        public void Dispose()
        {
            dataContext.Dispose();
        }
    }
}