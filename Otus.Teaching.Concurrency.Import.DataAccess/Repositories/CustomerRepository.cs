using System;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        DataContext dataContext;

        object locker = new object();

        public CustomerRepository()
        {
            dataContext = new DataContext();
        }

        public void AddCustomer(IEnumerable<Customer> customers)
        {
            lock (locker)
            {
                foreach (var item in customers)
                {
                    dataContext.Customers.Add(item);
                }
                dataContext.SaveChanges();
            }
        }

        public async Task AddCustomerAsync(IEnumerable<Customer> customers)
        {
            Console.WriteLine($"Async customer add in thread {System.Threading.Thread.CurrentThread.ManagedThreadId}");
            await Task.Run(() => AddCustomer(customers));

            //dataContext.Customers.AddRange(customers);
            //await dataContext.SaveChangesAsync();
            Console.WriteLine($"Async adding in thread {System.Threading.Thread.CurrentThread.ManagedThreadId} finished!");
        }


        public int AddCustomer(Customer customer)
        {
            lock (locker)
            {
                dataContext.Customers.Add(customer);
                dataContext.SaveChanges();
            }
            return 0;
        }

        public Task<Customer> GetUserAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddCustomerAsync(Customer customer)
        {
            throw new NotImplementedException();
        }

        public Task<List<Customer>> GetAllUsersAsync()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}