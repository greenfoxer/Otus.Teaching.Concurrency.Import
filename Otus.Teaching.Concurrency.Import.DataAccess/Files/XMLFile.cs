﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System.Xml.Linq;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Files
{
    public class XMLFile : IDataFile 
    {
        XDocument xmlDocument;

        IEnumerable<XElement> nodes;

        public XMLFile() { }

        public XMLFile(string path)
        {
            ReadFile(path);
        }

        public void ReadFile(string path)
        {
            xmlDocument = XDocument.Load(path);
            nodes = xmlDocument.Element("Customers").Element("Customers").Elements("Customer").OrderBy(w => int.Parse(w.Element("Id").Value));
        }

        public IEnumerable<XElement> GetPart(int StartId, int EndId)
        {
            //return nodes.Where(w => int.Parse(w.Element("Id").Value) >= StartId && int.Parse(w.Element("Id").Value) <= EndId);
            return nodes.Skip(StartId).Take(EndId - StartId).ToList(); ;
        }
    }
}
