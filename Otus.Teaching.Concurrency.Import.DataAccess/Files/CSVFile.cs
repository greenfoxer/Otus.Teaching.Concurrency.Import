﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System.IO;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Files
{
    public class CSVFile : IDataFile
    {
        List<string> data;

        public CSVFile() { }

        public CSVFile(string path)
        {
            ReadFile(path);
        }

        public void ReadFile(string path)
        {
            data = new List<string>(File.ReadLines(path));
        }
        
        public List<string> GetPart(int StartId, int EndId)
        { 
            return data.Skip(StartId).Take(EndId - StartId).ToList();
        }
    }
}
