﻿using System.Collections.Generic;
using System.Xml.Linq;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private IEnumerable<XElement> xmlElements;

        public XmlParser(IEnumerable<XElement> xmlElements)
        {
            this.xmlElements = xmlElements;
        }

        public List<Customer> Parse()
        {
            List<Customer> customers = new List<Customer>();
            foreach (XElement xel in xmlElements)
                customers.Add(new Customer {
                    Id = int.Parse(xel.Element("Id").Value),
                    FullName = xel.Element("FullName").Value,
                    Email = xel.Element("Email").Value,
                    Phone = xel.Element("Phone").Value
                });
         
            return customers;
        }
    }
}