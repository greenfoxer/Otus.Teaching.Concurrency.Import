﻿using System.Collections.Generic;
using System.Xml.Linq;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CSVParser
        : IDataParser<List<Customer>>
    {
        private IEnumerable<string> csvElements;

        public CSVParser(IEnumerable<string> csvElements)
        {
            this.csvElements = csvElements;
        }

        public List<Customer> Parse()
        {
            List<Customer> customers = new List<Customer>();
            foreach (string cel in csvElements)
            {
                string[] fields = cel.Split(',');
                customers.Add(new Customer { 
                    Id = int.Parse(fields[0]) ,
                    FullName = fields[1], Email = fields[2], 
                    Phone = fields[3]
                });
            }
            return customers;
        }
    }
}