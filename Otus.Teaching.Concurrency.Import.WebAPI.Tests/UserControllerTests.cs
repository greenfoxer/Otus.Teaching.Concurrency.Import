﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebAPI.Model;
using Otus.Teaching.Concurrency.Import.WebAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
//using Microsoft.AspNetCore.Http;
using System.Web.Http.Results;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Tests
{
    [TestFixture]
    class UserControllerTests
    {
        IUserService _userService;
        UserController _userController;
        
        public UserControllerTests()
        {
            _userService = new FakeUserService();
            _userController = new UserController(_userService);
        }
        [Test]
        public void GetAllUser_ResultTest()
        {
            //Act
            var result = _userController.Get().Result;
            //Assert
            Assert.IsInstanceOf(typeof(OkObjectResult), result, "Wrong statuscode type!");
        }
        [Test]
        public void GetAllUser_CollectionTypeTest()
        {
            //Act
            var result = _userController.Get().Result as OkObjectResult;
            //Assert
            Assert.IsInstanceOf(typeof(List<Customer>), result.Value,"Wrong collection type!");
        }
        [Test]
        public void GetAllUser_CollectionLengthTest()
        {
            //Arrange 
            int expectedCount = (_userService as FakeUserService)._database.Count;
            //Act
            var result = _userController.Get().Result as OkObjectResult ;
            var collection = result.Value as List<Customer>;
            //Assert
            Assert.AreEqual(expectedCount,collection.Count,"Wrong count of collection!");
        }
        [Test]
        public void GetUser_TypeTest()
        {
            //Arrange
            int requestedId = 1;
            //Act
            var result = _userController.Get(requestedId).Result as ObjectResult;
            //Assert
            Assert.IsInstanceOf(typeof(Customer), result.Value, "Wrong type returned by id!");
        }
        [Test]
        public void GetUser_WrongIdTest()
        {
            //Arrange
            int requestedId = 0;
            int statusCode = 404;
            //Act
            var result = _userController.Get(requestedId).Result as Microsoft.AspNetCore.Mvc.NotFoundResult;
            //Assert
            Assert.AreEqual(statusCode, result.StatusCode);
        }
        [Test]
        public void GetUser_ValueTest()
        {
            //Arrange
            int requestedId = 1;
            //Act
            var result = _userController.Get(requestedId).Result as OkObjectResult;
            //Assert
            Assert.AreEqual(requestedId, (result.Value as Customer).Id, "Wrong returned data!");
        }
        [Test]
        public void AddUser_ExistTest()
        {
            //Arrange
            int expectedCode = 409;
            Customer newCustomer = new Customer() { Id = 1, FullName = "Test Fourth", Email = "test_fourth@gmail.com", Phone = "4-160-832-123" } ;
            //Act
            var result = _userController.Post(newCustomer).Result as ObjectResult;
            //Assert
            Assert.AreEqual(expectedCode, result.StatusCode, "Wrong returned data!");
        }
        [Test]
        public void AddUser_FaultTest()
        {
            //Arrange
            int expectedCode = 500;
            Customer newCustomer = new Customer() { Id = 1, FullName = "Test Fourth", Email = "test_fourth@gmail.com", Phone = "4-160-832-123" };
            //Act
            (_userService as FakeUserService).IsFakeError = true;
            var result = _userController.Post(newCustomer).Result as ObjectResult;
            (_userService as FakeUserService).IsFakeError = false;
            //Assert
            Assert.AreEqual(expectedCode, result.StatusCode, "Wrong returned data!");
        }
        [Test]
        public void AddUser_NormalTest()
        {
            //Arrange
            int expectedCode = 200;
            Customer newCustomer = new Customer() { Id = 4, FullName = "Test Fourth", Email = "test_fourth@gmail.com", Phone = "4-160-832-123" };
            //Act
            var result = _userController.Post(newCustomer).Result as ObjectResult;
            //Assert
            Assert.AreEqual(expectedCode, result.StatusCode, "Wrong returned data!");
        }
    }
}
