﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebAPI.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Net;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Tests
{
    public class FakeUserService : IUserService
    {
        public List<Customer> _database;
        public bool IsFakeError { get; set; } = false;

        public FakeUserService()
        {
            _database = new List<Customer>() {
            new Customer(){ Id = 1, FullName = "Test First", Email = "test_first@gmail.com", Phone = "1-213-345-123" },
             new Customer(){ Id = 2, FullName = "Test Second", Email = "test_second@gmail.com", Phone = "2-789-345-123" },
              new Customer(){ Id = 3, FullName = "Test Third", Email = "test_thirdt@gmail.com", Phone = "3-160-345-123" },
            };
        }

        public async Task<int> AddUserAsync(Customer user)
        {
            if (IsFakeError)
                throw new Exception();
            var item = _database.FirstOrDefault(p => p.Id == user.Id);
            if (item == null)
            {
                _database.Add(user);
                return 0;
            }
            else
                return 1;
        }

        public void Dispose()
        {
            
        }

        public async Task<IEnumerable<Customer>> GetAllUsersAsync()
        {
            return _database;
        }

        public async Task<Customer> GetUserAsync(int id)
        {
            return _database.FirstOrDefault(p => p.Id == id);
        }
    }
}
