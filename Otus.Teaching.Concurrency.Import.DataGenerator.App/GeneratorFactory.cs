using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using CSVDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CSVGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount)
        {
            if (fileName.ToLower().EndsWith(".csv"))
                return new CSVDataGenerator(fileName, dataCount);
            return new XmlDataGenerator(fileName, dataCount); // �� ��������� ���������� xml
        }
    }
}