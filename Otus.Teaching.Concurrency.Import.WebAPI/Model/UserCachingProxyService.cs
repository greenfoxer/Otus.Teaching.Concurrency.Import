﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Model
{
    public class UserCachingProxyService : IUserService
    {
        private ICustomerRepository repository = null;
        Dictionary<int,Customer> _cache;
        IUserService RealServer;

        public UserCachingProxyService(ICustomerRepository repo)
        {
            repository = repo;
            _cache = new Dictionary<int, Customer>();
        }

        public async Task<int> AddUserAsync(Customer user)
        {
            return await repository.AddCustomerAsync(user);
        }

        public void Dispose()
        {
            repository.Dispose();
        }

        public async Task<IEnumerable<Customer>> GetAllUsersAsync()
        {
            return await repository.GetAllUsersAsync();
        }

        public async Task<Customer> GetUserAsync(int id)
        {
            if (_cache.ContainsKey(id))
                return _cache[id];
            else
                _cache.Add(id, await repository.GetUserAsync(id));
            return _cache[id];
        }
    }
}
