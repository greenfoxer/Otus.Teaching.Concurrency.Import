﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
namespace Otus.Teaching.Concurrency.Import.WebAPI.Model
{
    public interface IUserService: IDisposable
    {
        Task<IEnumerable<Customer>> GetAllUsersAsync();
        Task<Customer> GetUserAsync(int id);
        Task<int> AddUserAsync(Customer user);
    }
}
