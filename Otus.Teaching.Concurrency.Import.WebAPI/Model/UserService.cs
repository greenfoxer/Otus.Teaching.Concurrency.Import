﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;


namespace Otus.Teaching.Concurrency.Import.WebAPI.Model
{
    public class UserService : IUserService
    {
        private ICustomerRepository repository = null;

        public UserService(ICustomerRepository repo)
        {
            repository = repo;
        }

        public async Task<int> AddUserAsync(Customer user)
        {
            return await repository.AddCustomerAsync(user);
        }

        public void Dispose()
        {
            repository.Dispose();
        }

        public async Task<IEnumerable<Customer>> GetAllUsersAsync()
        {
            return await repository.GetAllUsersAsync();
        }

        public async Task<Customer> GetUserAsync(int id)
        {
            return await repository.GetUserAsync(id);
        }
    }
}
