﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using System.Net;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        Settings options;
        
        public DataController(IOptions<Settings> options)
        {
            this.options = options.Value;
        }

        [HttpGet()]
        public ContentResult Get(int count)
        {
            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = "<html><body><h1>This is method for generation DataBase.</h1> Using /api/data/{n} where n - number of generated users. <p> You will be redirected to /api/user after successful user generation.</body></html>"
            };
        }

        [HttpGet("{count}")]
        public async Task<string> GenerateDB(int count)
        {
            int status = await Task.Run(()=> Generate(count));
            Response.StatusCode = status;
            if (status == 200)
            {
                Response.Redirect("/api/user");
                return "Data have been added!";
            }
            else
                return "Something goes wrong!";
        }

        int Generate(int num)
        {
            string[] arguments = { "-o", options.ConnectionString,"-n",num.ToString(),"-p","7" };
            FileInfo fileInfo = new FileInfo(options.Generator);
            try
            {
                Loader.Program.Main(arguments);
            }
            catch(Exception e)
            {
                return 500;
            }
            return 200;
        }
    }
}