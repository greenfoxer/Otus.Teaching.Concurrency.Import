﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Otus.Teaching.Concurrency.Import.WebAPI.Model;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Otus.Teaching.Concurrency.Import.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userRepository;

        public UserController(IUserService repo)
        {
            userRepository = repo;
        }
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await userRepository.GetAllUsersAsync();
            return Ok(result);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                Console.WriteLine("Request for id: " + id);
                var t = await userRepository.GetUserAsync(id);
                if (t == null)
                    return NotFound();
                return Ok(t);
            }
            catch (Exception e)
            {
                //Response.StatusCode = 500; // ошибка сервера
                return StatusCode(500); 
            }   
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Customer value)
        {
            try
            {
                Console.WriteLine("Incoming POST body: "+value);
                int isInserted = await userRepository.AddUserAsync(value);
                if (isInserted == 1)
                    return StatusCode(409,"Already in db!"); // если пользователь уже в базе - 404 по условию
                return Ok("Success!");
            }
            catch (Exception)
            {
                //Response.StatusCode = 500; // ошибка сервера
                return StatusCode(500,"Fail!");
            }
            //return "Fail!";
        }

        // не предусмотрены заданием
        /*
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
