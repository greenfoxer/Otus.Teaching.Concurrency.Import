using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.WebAPI.Model;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddOptions();

            services.Configure<Settings>(Configuration.GetSection("DbConnection"));// �������� ��������� ��� ������� �������� ������������� ���� ������

            string conString = Configuration.GetSection("DbConnection:ConnectionString").Value;
            IOptions<Settings> options;
            services.AddScoped<IUserService, UserService>(); // DI ��� ������� ������� � �����������

            services.AddDbContext<DataContext>(opt => opt.UseSqlite(conString));

            // ������������ ������� ��������
            //services.AddTransient<ICustomerRepository, CustomerRepositoryNoLock>();

            // ������������ ������-������
            services.AddSingleton<CustomerCacheProxy>();
            services.AddSingleton<ICustomerRepository, CustomerRepositoryNoLock>();
            services.Decorate<ICustomerRepository, CustomerCacheProxy>();

            //services.AddTransient<ICustomerRepository>(provider => new CustomerRepositoryNoLock(conString)); // DI ��� ���������� ����������� CustomerRepositoryNoLock

            services.AddSwaggerDocument();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseOpenApi();

            app.UseSwaggerUi3();
        }
    }
}
