﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebAPI
{
    public class Settings
    {
        public string ConnectionString { get; set; }
        public string Generator { get; set; }
    }
}
