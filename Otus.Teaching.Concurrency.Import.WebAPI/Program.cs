using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.Concurrency.Import.WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>().UseKestrel(so =>
                   so.ListenAnyIP(5002, listenOptions =>
                   {
                       listenOptions.UseHttps(httpsOptions =>
                       {
                           var localhostCert = CertificateLoader.LoadFromStoreCert(
                               "localhost", "My", StoreLocation.CurrentUser,
                               allowInvalid: true);

                           var certs = new Dictionary<string, X509Certificate2>(
                               StringComparer.OrdinalIgnoreCase);
                           certs["localhost"] = localhostCert;

                           httpsOptions.ServerCertificateSelector = (connectionContext, name) =>
                           {
                               if (name != null && certs.TryGetValue(name, out var cert))
                               {
                                   return cert;
                               }

                               return localhostCert;
                           };
                       });
                   }));
                });
    }
}