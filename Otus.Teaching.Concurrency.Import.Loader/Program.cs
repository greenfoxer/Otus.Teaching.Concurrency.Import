﻿using System;
using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using CommandLine;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class Program
    {
        public class Options // описание параметров командной строки и их критичность. например -o и -n критичные.
        {
            [Option('g', "generator", Required = false, HelpText = "Set generator *.exe")]
            public string Generator { get; set; }
            [Option('o', "output", Required = true, HelpText = "Required output file with extension.")]
            public string Output { get; set; }
            [Option('n', "number", Required = true, HelpText = "Required number of data to generate.")]
            public int Number { get; set; }
            [Option('t', "threads", Required = false, HelpText = "Set number of threads to processing. It is equal to number of ranges.")]
            public int Threads { get; set; }
            [Option('p', "pool", Required = false, HelpText = "Set number of threads to processing. It is equal to number of ranges.")]
            public int ThreadPool { get; set; }
            [Option('a', "async", Required = false, HelpText = "Set flag for async generation.")]
            public int Async { get; set; }
        }

        public static void Main(string[] args)
        {
            StrategyRequest strategyRequest = new StrategyRequest(); // класс входных параметров для принятия решения в Стратегии

            // Подключаемая библиотека для управления аргуиментам командной строки, маппится на StrategyRequest
            Parser.Default.ParseArguments<Options>(args).WithParsed<Options>(o =>
            {
                strategyRequest._dataFilePath = o.Output;
                if (!(strategyRequest._dataFilePath.ToLower().EndsWith(".xml") || strategyRequest._dataFilePath.ToLower().EndsWith(".csv")))
                    strategyRequest._dataFilePath = strategyRequest._dataFilePath + ".xml"; // если явно не указано расширение, до сами определим его, как xml
                strategyRequest._dataGenerator = o.Generator;
                strategyRequest._number = o.Number;
                strategyRequest._threads = o.Threads;
                strategyRequest._threadPool = o.ThreadPool;
                strategyRequest._async = o.Async;
            });

            GenerateCustomersDataFile(strategyRequest);
            
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            Console.WriteLine("####################START#####################");
            var loader = new LoaderContext(strategyRequest); // инициализация контекста выбора Стратегии
            loader.StartLoad();

            //Тестовые запуски для отображения результатов измерений на одном экране.
            #region Test_Measurements
            //Console.WriteLine("####################START THREADPOOL####################");
            //strategyRequest._threadPool = 7;
            //strategyRequest._threads = 0;
            //strategyRequest._async = 0;
            //var loader = new LoaderContext(strategyRequest);
            //loader.StartLoad();

            //Console.WriteLine("####################START THREADS####################");
            //strategyRequest._threadPool = 0;
            //strategyRequest._threads = 7;
            //strategyRequest._async = 0;
            //loader = new LoaderContext(strategyRequest);
            //loader.StartLoad();

            //Console.WriteLine("####################ASYNC####################");
            //strategyRequest._threadPool = 0;
            //strategyRequest._threads = 0;
            //strategyRequest._async = 7;
            //loader = new LoaderContext(strategyRequest);
            //loader.StartLoad();

            //Console.WriteLine("####################SINGLE THREAD####################");
            //strategyRequest._threadPool = 0;
            //strategyRequest._threads = 0;
            //strategyRequest._async = 0;
            //loader = new LoaderContext(strategyRequest);
            //loader.StartLoad();

            //Console.WriteLine("####################SINGLE THREAD ASYNC####################");
            //strategyRequest._threadPool = 0;
            //strategyRequest._threads = 0;
            //strategyRequest._async = 1;
            //loader = new LoaderContext(strategyRequest);
            //loader.StartLoad();
            #endregion
        }

        static void GenerateCustomersDataFile(StrategyRequest strategyRequest)
        {
            //strategyRequest._dataGenerator = @"c:\Users\user\source\repos\test\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
            //strategyRequest._isProcess = true;

            string[] arguments = { strategyRequest._dataFilePath, strategyRequest._number.ToString() };
            if (!string.IsNullOrEmpty(strategyRequest._dataGenerator))
            {
                FileInfo fileInfo = new FileInfo(strategyRequest._dataGenerator);
                strategyRequest._dataFilePath = $"{fileInfo.Directory.ToString()}\\{strategyRequest._dataFilePath}";
                Process process = new Process();
                process.StartInfo.FileName = strategyRequest._dataGenerator;
                process.StartInfo.Arguments = string.Join(' ',arguments);
                process.Start();
                Console.WriteLine($"External data generator has been started in process {process.Id}");
                process.WaitForExit();
                if (process.ExitCode == 0)
                    Console.WriteLine($"Data generation sucessfully ended!");
                else
                    Console.WriteLine($"Something goes wrong with exit code: {process.ExitCode}");
            }
            else
            {
                XmlGenerator.Program.Main(arguments);
            }
        }
    }
}