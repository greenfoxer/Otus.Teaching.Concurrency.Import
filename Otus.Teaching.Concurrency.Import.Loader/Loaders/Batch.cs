﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Linq;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Files;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class Batch
    {
        public IEnumerable<Customer> customers;
        public bool IsDone { get; set; }
        public int Start { get { return customers.Min(p => p.Id); } }
        public int End { get { return customers.Max(p => p.Id); } }
        public Batch(IEnumerable<Customer> customers)
        {
            this.customers = customers;
            IsDone = false;
        }
    }
}

