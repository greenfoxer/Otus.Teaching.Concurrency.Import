﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class ThreadPoolDataLoader : IDataLoader
    {
        public IBaseDataLoader BaseDataLoader { get; set; }
        public string FileName { get; set; }
        public int StartId { get; set; }
        public int EndId { get; set; }

        int PoolSize;

        public ThreadPoolDataLoader(IBaseDataLoader bdl, int poolsize)
        {
            BaseDataLoader = bdl;
            PoolSize = poolsize;
        }

        public void LoadData()
        {
            using (var t = new Otus.Teaching.Concurrency.Import.DataAccess.Repositories.CustomerRepositoryNoLock())
                t.TruncateTable(); // пересоздадим для начала бд и таблицу.

            BaseDataLoader.SetDataFile(FileName);

            int total = (EndId - StartId);
            int packSize = total / PoolSize;
            IBaseDataLoader[] dataLoaders = new IBaseDataLoader[PoolSize];
            for (int i = 0; i < PoolSize; i++)
            {
                var dataLoader = BaseDataLoader.Clone();
                dataLoader.SetFileName(FileName);
                //int currentStart = StartId + i * packSize;
                //int currentEnd = (i + 1) == PoolSize ? EndId : (StartId + (i + 1) * packSize - 1);
                //Console.WriteLine($"Start range {currentStart} - {currentEnd}");
                dataLoader.SetRange(StartId + i * packSize, (i + 1) == PoolSize ? EndId : (StartId + (i + 1) * packSize));
                dataLoaders[i] = dataLoader;
                //Thread.Sleep(500); // можно раскомментировать и посмотреть, что из пула потоков берутся одни и те же потоки
                ThreadPool.QueueUserWorkItem(LoaderInThreadPool, dataLoader);
            }
            WaitHandle.WaitAll(dataLoaders.Select(x => x.WaitHandle).ToArray());
        }

        private void LoaderInThreadPool(object item)
        {
            //Console.WriteLine($"Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            bool isFailed = false;
            int counter = 1;
            do
            {
                try
                {
                    ((IDataLoader)item).LoadData();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Attempt {counter} in thread {Thread.CurrentThread.ManagedThreadId} failed with error: {e.Message}");
                    counter++;
                    isFailed = true;
                }
            } while (isFailed && counter <= 1);
            var autoResetEvent = (AutoResetEvent)((IBaseDataLoader)item).WaitHandle;
            autoResetEvent.Set();
        }
    }
}
