﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class LoaderContext
    {
        IDataLoader DataLoader;

        public LoaderContext(StrategyRequest strategyRequest)
        {
            SetUpLoader(strategyRequest);
        }

        public void StartLoad()
        {
            Console.WriteLine("Start loading...");
            var timer = new Stopwatch();
            timer.Start();
            DataLoader?.LoadData();
            timer.Stop();
            Console.WriteLine($"Loading completed in {timer.Elapsed}");
        }

        private void SetUpLoader(StrategyRequest parameters)
        {
            IDataLoader currentDataLoader = null; // dataloader который мы будем вызывать
            IBaseDataLoader baseDataLoader; // dataloader, на основе которого будет реализовываться многопоточность
            if (parameters._dataFilePath.ToLower().EndsWith(".csv")) //если csv
                baseDataLoader = new CSVBaseDataLoader();
            else
                baseDataLoader = new XMLBaseDataLoader(); // иначе, согласно предыдущей логике - xml
            
            if (parameters._threads > 0)
                currentDataLoader = new ThreadDataLoader(baseDataLoader, parameters._threads);
            if (parameters._threadPool > 0)
                currentDataLoader = new ThreadPoolDataLoader(baseDataLoader, parameters._threadPool);
            if (parameters._async > 0)
                currentDataLoader = new AsyncLoader(baseDataLoader, parameters._async);

            if (currentDataLoader == null) // если не многопоточный случай, то current = base
                currentDataLoader = baseDataLoader; // работает за счет наследования интерфейсов

            DataLoader = currentDataLoader;

            DataLoader.SetFileName(parameters._dataFilePath);
            DataLoader.SetRange(0, parameters._number);
        }
    }
}
