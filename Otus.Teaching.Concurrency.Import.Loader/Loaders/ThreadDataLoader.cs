﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class ThreadDataLoader : IDataLoader
    {
        public IBaseDataLoader BaseDataLoader { get; set; }
        public string FileName { get; set; }
        public int StartId { get; set; }
        public int EndId { get; set; }

        int NumberOfRange;

        public ThreadDataLoader(IBaseDataLoader bdl, int threads)
        {
            BaseDataLoader = bdl;
            NumberOfRange = threads;
        }

        public void LoadData()
        {
            using (var t = new Otus.Teaching.Concurrency.Import.DataAccess.Repositories.CustomerRepositoryNoLock())
                t.TruncateTable(); // пересоздадим для начала бд и таблицу.

            BaseDataLoader.SetDataFile(FileName);
            int total = (EndId - StartId);
            int packSize = total / NumberOfRange;
            Thread[] dataLoaders = new Thread[NumberOfRange];
            for (int i = 0; i < NumberOfRange; i++)
            {
                var dataLoader = BaseDataLoader.Clone();
                dataLoader.SetFileName(FileName);
                dataLoader.SetRange(StartId + i * packSize, (i + 1) == NumberOfRange ? EndId : (StartId + (i + 1) * packSize));
                dataLoaders[i] = new Thread(() => SafeThreadCaller( dataLoader.LoadData , TheadExceptionHandler));
                dataLoaders[i].Start();
            }
            while (dataLoaders.Any(t => t.IsAlive)) { } // ждем, пока есть хотя бы один живой поток.
        }

        #region SafeThreadStartAndHandling
        void TheadExceptionHandler(Exception e, int attempt)
        {
            Console.WriteLine($"Thread data load attempt {attempt} in thread {Thread.CurrentThread.ManagedThreadId} failed with error: {e.Message}");
        }

        void SafeThreadCaller(Action method, Action<Exception,int> errorHandler)
        {
            bool isFailed = false;
            int counter = 1;
            do
            {
                try
                {
                    //Console.WriteLine($"Thread data load attempt {counter}"); 
                    method.Invoke();
                }
                catch (Exception e)
                {
                    errorHandler.Invoke(e, counter);
                    counter++;
                    isFailed = true;
                }
            } while (isFailed && counter <= 1);
        }
        #endregion
    }
}
