﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{ 
    class AsyncLoader :  IDataLoader
    {
        public IBaseDataLoader BaseDataLoader { get; set; }
        public string FileName { get; set; }
        public int StartId { get; set; }
        public int EndId { get; set; }
        int TaskStarted;

        public void LoadData()
        {
            using (var t = new Otus.Teaching.Concurrency.Import.DataAccess.Repositories.CustomerRepositoryNoLock())
                t.TruncateTable(); // пересоздадим для начала бд и таблицу.

            BaseDataLoader.SetDataFile(FileName);

            Task[] asyncTasks = new Task[TaskStarted];
            int total = (EndId - StartId);
            int packSize = total / TaskStarted;
            for (int i = 0; i < TaskStarted; i++)
            {
                var dataLoader = BaseDataLoader.Clone();
                dataLoader.SetFileName(FileName);
                //dataLoader.repository = new DataAccess.Repositories.CustomerRepository();
                dataLoader.SetRange(StartId + i * packSize, (i + 1) == TaskStarted ? EndId : (StartId + (i + 1) * packSize));
                asyncTasks[i] = SafeAsyncPosting(dataLoader.LoadDataAsync, AsyncExceptionHandler);
            }
            Console.WriteLine("Waiting for all async method will finished...");
            var timer = new Stopwatch();
            timer.Start();
            Task.WaitAll(asyncTasks);
            timer.Stop();
            Console.WriteLine($"All async method is done! The program was wait for {timer.Elapsed} !");
        }

        void AsyncExceptionHandler(Exception e, int attempt)
        {
            Console.WriteLine($"Attempt {attempt} failed with error: {e.Message}");
        }

        delegate Task AwaitDelegate();

        private async Task SafeAsyncPosting(AwaitDelegate method, Action<Exception, int> errorHandler)
        {
            bool isFailed = false;
            int counter = 1;
            do
            {
                try
                {
                    await Task.Run(()=>method.Invoke());
                }
                catch (Exception e)
                {
                    errorHandler.Invoke(e, counter);
                    counter++;
                    isFailed = true;
                }
            } while (isFailed && counter <= 3);
        }

        public AsyncLoader(IBaseDataLoader bdl, int asyncTasks)
        {
            TaskStarted = asyncTasks;
            BaseDataLoader = bdl;
        }
    }
}
