﻿
using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Text;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Linq;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Files;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class XMLBaseDataLoader : AbstractBaseLoader, IBaseDataLoader
    {
        public int StartId { get; set; }
        public int EndId { get; set; }
        public string FileName { get; set; }
        public WaitHandle WaitHandle { get; set; } = new AutoResetEvent(false);
        public IDataFile DataFile { get; set; }

        public XMLBaseDataLoader(int start, int finish)
        {
            StartId = start;
            EndId = finish;
        }

        public XMLBaseDataLoader()        
        {            //repository = new CustomerRepository();
        }

        public async Task LoadDataAsync()
        {
            await Task.Run(() => LoadData());
        }

        public void SetDataFile(string fileName)
        {
            DataFile = new XMLFile(fileName);
        }

        public void LoadData()
        {
            if (DataFile == null) // в случае однопоточного режима
            {
                SetDataFile(FileName);
                using (var t = new CustomerRepositoryNoLock())
                    t.TruncateTable();
            }
            if (batches == null)    // если повторная попытка запуска потока, то не перераспределяем batches
            {
                var raw_customers = (DataFile as XMLFile).GetPart(StartId, EndId);
                var xmlParser = new XmlParser(raw_customers);
                IEnumerable<Customer> customers = xmlParser.Parse().OrderBy(p => p.Id);
                GenerateBatches(customers);
            }
            BatchPosting();
        }

        public IBaseDataLoader Clone()
        {
            var result = (XMLBaseDataLoader)this.MemberwiseClone();
            ((IBaseDataLoader)result).SetFileName(FileName);
            result.WaitHandle = new AutoResetEvent(false);
            return result; // отмечу, что для ICustomerRepository используем shallow copy, а для WaitHandle - deep copy.
        }
    }
}
