﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Linq;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Files;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class AbstractBaseLoader // хранит общие методы для IBaseDataLoader-ов
    {
        protected Batch[] batches;
        public void GenerateBatches(IEnumerable<Customer> customers)
        {
            int batchSize = 10000;
            int rangeNum = (customers.Count() / batchSize)==0 ? 1: (customers.Count() / batchSize);
            
            batches = new Batch[rangeNum];
            for (int i = 0; i < rangeNum; i ++)
            {
                var batch = customers.Skip(i*batchSize).Take((i + 1) == rangeNum ? (customers.Count() - rangeNum*i + 1) + batchSize : batchSize);
                batches[i] = new Batch(batch);
            }
        }

        public async Task BatchPostingAsync()
        {
            for (int i = 0; i < batches.Length; i++)
            {
                if (!batches[i].IsDone)
                {
                    //Console.WriteLine($"Batch {batches[i].Start} - {batches[i].End}");
                    bool isFailed = false;
                    int counter = 1;
                    do
                    {
                        try
                        {
                            using (var repo = new CustomerRepositoryNoLock())
                            {
                                 await repo.AddCustomerAsync(batches[i].customers);
                            }
                        }
                        catch (Microsoft.Data.Sqlite.SqliteException e)
                        {
                            Console.WriteLine($"Batch {batches[i].Start} - {batches[i].End} attempt {counter} in thread {Thread.CurrentThread.ManagedThreadId} failed by lock!!!");
                            counter++;
                            isFailed = true;
                            Thread.Sleep(1000);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"Batch {batches[i].Start} - {batches[i].End} attempt {counter} in thread {Thread.CurrentThread.ManagedThreadId} failed by unique id!!!");
                            counter++;
                            isFailed = true;
                            Thread.Sleep(1000);
                        }
                    } while (isFailed && counter <= 2);
                    if (isFailed) // иногда некоторые батчи уходят в дедлок
                    {
                        try
                        {
                            await Task.Run(() =>
                            {
                                using (var repo = new CustomerRepositoryNoLock())
                                {
                                    int reload = 0;
                                    foreach (var customer in batches[i].customers)
                                        reload += repo.AddCustomerAsync(customer).Result;
                                    Console.WriteLine($"{reload} from {batches[i].customers.Count()} of this batch already in DB");
                                }
                            });
                            isFailed = false;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Wrong!");
                        }
                    }
                    if (isFailed)
                        throw new Exception("Batch posting failed.");
                    else
                        batches[i].IsDone = true;
                }
            }
        }

        public void BatchPosting()
        {
            for (int i = 0; i < batches.Length; i++)
            {
                if (!batches[i].IsDone)
                {
                    //Console.WriteLine($"Batch {batches[i].Start} - {batches[i].End}");
                    bool isFailed = false;
                    int counter = 1;
                    do
                    {
                        try
                        {
                            using (var repo = new CustomerRepositoryNoLock())
                            {
                                repo.AddCustomer(batches[i].customers);
                            }
                        }
                        catch (Microsoft.Data.Sqlite.SqliteException e)
                        {
                            Console.WriteLine($"Batch {batches[i].Start} - {batches[i].End} attempt {counter} in thread {Thread.CurrentThread.ManagedThreadId} failed by lock!!!");
                            counter++;
                            isFailed = true;
                            Thread.Sleep(1000);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"Batch {batches[i].Start} - {batches[i].End} attempt {counter} in thread {Thread.CurrentThread.ManagedThreadId} failed by unique id!!!");
                            counter++;
                            isFailed = true;
                            Thread.Sleep(1000);
                        }
                    } while (isFailed && counter <= 2);
                    if (isFailed) // иногда некоторые батчи уходят в дедлок
                    {
                        try
                        {
                            using (var repo = new CustomerRepositoryNoLock())
                            {
                                int reload = 0;
                                foreach(var customer in batches[i].customers)
                                    reload += repo.AddCustomer(customer);
                                Console.WriteLine($"{reload} from {batches[i].customers.Count()} of this batch already in DB");
                            }
                            isFailed = false;
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("Wrong!");
                        }
                    }
                    if (isFailed)
                        throw new Exception("Batch posting failed.");
                    else
                        batches[i].IsDone = true;
                }
            }
        }
    }
}
