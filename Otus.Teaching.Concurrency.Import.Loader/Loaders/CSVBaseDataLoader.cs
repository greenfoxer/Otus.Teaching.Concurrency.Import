﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Linq;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Files;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    class CSVBaseDataLoader : AbstractBaseLoader, IBaseDataLoader
    {
        public int StartId { get; set; }
        public int EndId { get; set; }
        public string FileName { get; set; }
        public WaitHandle WaitHandle { get; set; }
        public IDataFile DataFile { get; set; }

        public CSVBaseDataLoader(int start, int finish)
        {
            StartId = start;
            EndId = finish;
        }

        public CSVBaseDataLoader() {
           // repository = new CustomerRepository();
        }

        public async Task LoadDataAsync()
        {
            if (DataFile == null)
            {
                SetDataFile(FileName);
                using (var t = new CustomerRepositoryNoLock())
                    t.TruncateTable();
            }
            if (batches == null)
            {
                var raw_customers = (DataFile as CSVFile).GetPart(StartId, EndId);
                CSVParser csvParser = new CSVParser(raw_customers);
                IEnumerable<Customer> customers = csvParser.Parse().OrderBy(p => p.Id);
                GenerateBatches(customers);
            }
            await BatchPostingAsync();
        }

        public void SetDataFile(string fileName)
        {
            DataFile = new CSVFile(fileName);
        }


        public void LoadData()
        {
            if (DataFile == null)
            {
                SetDataFile(FileName);
                using (var t = new CustomerRepositoryNoLock())
                    t.TruncateTable();
            }
            if (batches == null)
            {
                var raw_customers = (DataFile as CSVFile).GetPart(StartId, EndId);
                CSVParser csvParser = new CSVParser(raw_customers);
                IEnumerable<Customer> customers = csvParser.Parse().OrderBy(p => p.Id);
                GenerateBatches(customers);
            }
            BatchPosting();
        }

        public IBaseDataLoader Clone()
        {
            var result = (CSVBaseDataLoader)this.MemberwiseClone(); 
            ((IBaseDataLoader)result).SetFileName(FileName);
            result.WaitHandle = new AutoResetEvent(false);
            return result; // отмечу, что для ICustomerRepository используем shallow copy, а для WaitHandle - deep copy.
        }
    }
}
