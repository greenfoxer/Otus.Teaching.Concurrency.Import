﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class StrategyRequest
    {
        public string _dataFilePath { get; set; }
        public bool _isProcess { get; set; }
        public int _number { get; set; }
        public int _threads { get; set; }
        public string _dataGenerator { get; set; }
        public int _threadPool { get; set; }
        public string _fullPath { get; internal set; }
        public int _async { get; set; }
    }
}
