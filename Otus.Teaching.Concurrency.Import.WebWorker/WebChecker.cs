﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.WebWorker
{
    class WebChecker : IDisposable
    {
        public async Task<string> CheckUser(string id)
        {
            try
            {
                HttpClient client = new HttpClient();
                string server = @"https://localhost:5002/api/user/" + id;
                var result = await client.GetAsync(server);
                var str = result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    return "OK. Satus 200. Data: " + str.Result;
                else
                    return "Failed with code: " + ((int)result.StatusCode).ToString();
            }
            catch (Exception e )
            {
                return e.Message;
            }
            
        }

        public Customer GenerateOne(int id)
        {
            var result = RandomCustomerGenerator.GenerateOne(id);
            result.Id = id;
            return result;
        }

        public async Task<string> PostUser(Customer customer)
        {
            try
            {
                HttpClient client = new HttpClient();
                string server = @"https://localhost:5002/api/user";
                var result = await client.PostAsync(server, new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json"));
                var str = result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    return "OK. Satus 200.";
                else
                    return "Failed with code: " + ((int)result.StatusCode).ToString();
            }
            catch (Exception e)
            {
                return e.Message;

            }
        }

        async Task RunBatch(int size, int range)
        {
            string request;
            HttpClient client = new HttpClient();
            Random randomizer = new Random();
            for (int i = 0; i < size; i++)
            {
                request = @"https://localhost:5002/api/user/" + randomizer.Next(1, range).ToString();
                await client.GetAsync(request);
            }
        }

        public async Task<string> TestPerformance()
        {
            int range = 99;// 49999;
            int attempts = 1_000;
            int threads = 200;
            HttpClient client = new HttpClient();
            Stopwatch timer = new Stopwatch();
            string request;
            timer.Start();
            List<Task> connections = new List<Task>();
            Random randomizer = new Random();
            for (int i = 0; i < threads; i++)
            {
                connections.Add(RunBatch(attempts, range));
            }
            Task.WaitAll(connections.ToArray());
            timer.Stop();
            return $"Performance test in {attempts} attempts in {threads} async threads without timeouts.\nTotal: {attempts*threads} requests with ID's range [0,{range}].\nElapsed: {timer.Elapsed}";
        }

        public void Dispose()
        {
        }
    }
}
