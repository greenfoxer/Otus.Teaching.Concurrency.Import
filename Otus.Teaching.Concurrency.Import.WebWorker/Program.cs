﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
namespace Otus.Teaching.Concurrency.Import.WebWorker
{
    class Program
    {
        static async void MakeGet()
        {
            int parsed;
            Console.WriteLine("\nEnter ID to check:");
            string id = Console.ReadLine();
            if (int.TryParse(id, out parsed))
            {
                using (var checker = new WebChecker())
                {
                    //Console.WriteLine(await checker.CheckUser(id));
                    Console.WriteLine(Task.Run(() => checker.CheckUser(id)).Result);
                }
            }
        }
        static void MakePost()
        {
            int parsed;
            Console.WriteLine("\nEnter ID to inserted user:");
            string id = Console.ReadLine();
            if (int.TryParse(id, out parsed))
            {
                using (var checker = new WebChecker())
                {
                    var user = checker.GenerateOne(parsed);
                    Console.WriteLine(Task.Run(() => checker.PostUser(user)).Result);
                }
            }
        }

        static void Main(string[] args)
        {
            var choise = "put some ID there";
            while (string.Compare(choise, "exit", true) != 0) // проверка 
            {
                Console.WriteLine("\nOptions: \n 1: Check ID.\n 2: Post new user.\n 3: Perfomance testing.\n exit: Exit.");
                Console.Write("Input: ");
                choise = Console.ReadLine();
                switch (choise)
                {
                    case "1": MakeGet(); break;
                    case "2": MakePost(); break;
                    case "3": Perf(); break;
                    default:
                        break;
                }
            }
        }

        private static async void Perf()
        {
            WebChecker checker = new WebChecker();
            //string result = await checker.TestPerformance();
            Console.WriteLine(await checker.TestPerformance());
        }
    }
}
