﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Parsers;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IBaseDataLoader : IDataLoader
    {
        IBaseDataLoader Clone();
        public WaitHandle WaitHandle { get; set; }
        System.Threading.Tasks.Task LoadDataAsync();
        IDataFile DataFile { get;set; }
        void SetDataFile(string fileName);
        void GenerateBatches(IEnumerable<Customer> customers);
    }
}
