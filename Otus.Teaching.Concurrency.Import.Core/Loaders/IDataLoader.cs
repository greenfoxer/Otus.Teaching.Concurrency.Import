﻿using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader
    {
        string FileName { get; set; }
        int StartId { get; set; }
        int EndId { get; set; }
        void LoadData();
        void SetFileName(string fn)
        {
            FileName = fn;
        }
        void SetRange(int start, int finish)
        {
            StartId = start;
            EndId = finish;
        }
    }
}