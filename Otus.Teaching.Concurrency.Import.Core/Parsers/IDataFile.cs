﻿
namespace Otus.Teaching.Concurrency.Import.Core.Parsers
{

    public interface IDataFile
    {
        
        void ReadFile(string path);
    }
}
