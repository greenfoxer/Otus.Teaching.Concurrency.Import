namespace Otus.Teaching.Concurrency.Import.Handler.Data
{
    public interface IDataGenerator
    {
        void Generate();
        System.Threading.Tasks.Task GenerateAsync();
    }
}