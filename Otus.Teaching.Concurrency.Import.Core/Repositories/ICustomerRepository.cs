using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository: IDisposable
    {
        int AddCustomer(Customer customer);
        void AddCustomer(IEnumerable<Customer> customers);
        Task AddCustomerAsync(IEnumerable<Customer> customers);
        Task<int> AddCustomerAsync(Customer customer);
        Task<Customer> GetUserAsync(int id);
        Task<List<Customer>> GetAllUsersAsync();
    }
}